from ubuntu as builder
run apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*
workdir /usr/local/bin
run curl http://ariis.it/link/repos/gscholar-rss/bin/linux_64.tar.gz | tar xz

from ubuntu
run apt-get update && apt-get install -y ca-certificates && rm -rf /var/lib/apt/lists/*
copy --from=builder /usr/local/bin/gscholar-rss /usr/local/bin/gscholar-rss

ENV LC_ALL C.UTF-8
entrypoint ["/usr/local/bin/gscholar-rss"]
